Generate keys for 2 node like (node names would be Node1, Node2, etc):
```
python -m pzmq.generate_keys 2
```
To generate keys for a client, named `c1`:
```
python -m pzmq.generate_keys c1
```
Adding `--clean` flag clears existing keys.


Run a node as (here node with name Node1 is being run):
```
python -m pzmq.node Node1
```

Have a look at config.py for different flags and their description.


Nodes listen on ports 9700+ for nodes and ports 9800+ for clients, So if you are running 10 nodes, 10 ports will be used in 9700 range, and 10 ports will be 
used 9800 range. So make sure that in your firewall or AWS security group, TCP traffic is allowed for 9700-9900 


#To run a set of nodes and then run use clients to send requests to them

**To run 10 nodes locally on 10 different terminal sessions** 

1. Run, `python -m pzmq.generate_keys 10` once
2. Make sure `RunLocalNodes` in config.py is set to `True`
3. Make sure `RestrictedNodeCount` in config.py is set to `10`, otherwise it will assume that an attempt is being 
made to run as many nodes as keys in `public_keys` 
4. Now in 10 different 10 terminals, run `python -m pzmq.node Node1`, `python -m pzmq.node Node2`, and so on
5. Now run `run_clients.py` as `python -m pzmq.run_clients`. You can run this script from your IDE too


**To run 10 nodes on 10 different machines** 

1. Edit `locations.py` so that it contains IP of the 10 machines, so update values of `Node1` till `Node10`.
2. Run, `python -m pzmq.generate_keys 10` once on each machine you want to run
3. Make sure `RunLocalNodes` in config.py is set to `False` on each machine
4. Make sure `RestrictedNodeCount` in config.py is set to `10`, otherwise it will assume that an attempt is being 
made to run as many nodes as keys in `public_keys` 
5. Now on each of the 10 machines, run `python -m pzmq.node Node1`, `python -m pzmq.node Node2`, and so on
6. Now run `run_clients.py` as `python -m pzmq.run_clients` on your machine. You can run this script from your IDE too


#TODO:

Create a class `Peer` that supports encrypted and digitally signed communication, the signing part should be optional.
Each `Peer` should support both promiscuous and non-promiscuous modes of communication on different sockets.
Each `Peer` can be given a set of `Peer`s that it is supposed to make connections too.
Both node and client should extend `Peer`
