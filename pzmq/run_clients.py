import asyncio

import zmq.asyncio

from pzmq.client import OldClient as Client


loop = zmq.asyncio.ZMQEventLoop()
loop.set_debug(True)
asyncio.set_event_loop(loop)

numClients = 10
clients = []
for i in range(1, numClients+1):
    client = Client('c{}'.format(i), reqDelay=0)
    clients.append(client)
    client.start()
    asyncio.ensure_future(client.run())

loop.run_until_complete(asyncio.sleep(60))

totalReqs = 0
totalAcks = 0
totalReps = 0

for c in clients:
    a, r = c.stop()
    reqs = len(c.requests)
    print('{} sent {} requests, got {} acks and got {} replies'.format(c, reqs,
                                                                       a, r))
    totalReqs += reqs
    totalAcks += a
    totalReps += r

print('Total num of requests {}'.format(totalReqs))
print('Average num of requests {}'.format(int(totalReqs/numClients)))
print('Total num of Acks {}'.format(totalAcks))
print('Average num of Acks {}'.format(int(totalAcks/numClients)))
print('Total num of Replies {}'.format(totalReps))
print('Average num of Replies {}'.format(int(totalReps/numClients)))


# pending = asyncio.Task.all_tasks()
# try:
#     loop.run_until_complete(asyncio.gather(*pending))
# # except asyncio.CancelledError:
# except:
#     pass
# loop.close()
