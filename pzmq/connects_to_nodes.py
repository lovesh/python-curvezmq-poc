import asyncio
import zmq
from zmq.utils import z85

from pzmq.util import getNodeListenerPort, getClientListenerPort, unow
import pzmq.locations as locations
import pzmq.config as config


class ConnectsToNodes:
    lastTried = None

    def connectNode(self, name: str, pubkey, asNode):
        typ = zmq.DEALER
        public, secret = self.selfKeys
        sock = self.nodeCtx.socket(typ)
        sock.curve_secretkey = secret
        sock.curve_publickey = public
        sock.curve_serverkey = pubkey
        sock.identity = public
        print('{} setting identity for node {} as {}'.format(self, name, sock.identity))
        port = getNodeListenerPort(name) if asNode else getClientListenerPort(name)
        if not config.RunLocalNodes:
            host = locations.__dict__.get(name)
            if not host:
                raise RuntimeError("Host not found for {}".format(name))
        else:
            host = '127.0.0.1'
        addr = 'tcp://{}:{}'.format(host, port)
        sock.connect(addr)
        self.otherNodes[name] = sock
        print('{} will look for {} at {}'.format(self, name, addr))
        return sock

    @asyncio.coroutine
    def maintainNodeConnections(self):
        if len(self.connectedNodes) < len(self.otherNodes):
            now = unow()
            if not self.lastTried or (self.lastTried + 5 < now):
                disconnects = set(self.otherNodes.keys()).difference(set(self.connectedNodes))
                print('{} looking for {}'.format(self, ','.join(disconnects)))
                yield from self.pingNodes(nodes=disconnects)
                self.lastTried = now
