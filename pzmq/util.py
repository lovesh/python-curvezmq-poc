import ctypes
import datetime
import os
import random
import string
from binascii import unhexlify, hexlify

from base58 import b58encode
from raet.nacling import Signer

from libnacl import crypto_sign_seed_keypair, crypto_box_SECRETKEYBYTES, nacl, \
    crypto_box_PUBLICKEYBYTES
from zmq.auth.certs import _write_key_file, _cert_public_banner, \
    _cert_secret_banner
from zmq.utils import z85

from pzmq import config


def createCertsFromKeys(key_dir, name, public_key, secret_key=None, metadata=None):
    base_filename = os.path.join(key_dir, name)
    secret_key_file = "{0}.key_secret".format(base_filename)
    public_key_file = "{0}.key".format(base_filename)
    now = datetime.datetime.now()

    _write_key_file(public_key_file,
                    _cert_public_banner.format(now),
                    public_key)

    _write_key_file(secret_key_file,
                    _cert_secret_banner.format(now),
                    public_key,
                    secret_key=secret_key,
                    metadata=metadata)

    return public_key_file, secret_key_file


def randomSeed(size=32):
    return ''.join(random.choice(string.hexdigits)
                   for _ in range(size)).encode()


def createKeysFromEd22519(key_dir, name, seed=None):
    seed = seed or randomSeed()
    public_key, secret_key = crypto_sign_seed_keypair(seed)
    public_key, secret_key = ed25519PkToCurve25519(public_key), ed25519SkToCurve25519(secret_key)
    return createCertsFromKeys(key_dir, name, z85.encode(public_key), z85.encode(secret_key))
    # return createCertsFromKeys(key_dir, name, b58encode(public_key), b58encode(secret_key))


def seedFromHex(seed):
    if len(seed) == 64:
        try:
            return unhexlify(seed)
        except:
            pass


def cleanSeed(seed=None):
    if seed:
        bts = seedFromHex(seed)
        if not bts:
            if isinstance(seed, str):
                seed = seed.encode('utf-8')
            bts = bytes(seed)
            if len(seed) != 32:
                RuntimeError('seed length must be 32 bytes')
        return bts


def getEd25519AndCurve25519Keys(seed=None):
    if seed:
        seed = cleanSeed(seed)
    signer = Signer(seed)
    sigkey, verkey = signer.keyhex, signer.verhex
    prikey, pubkey = b58encode(ed25519SkToCurve25519(signer.keyraw)).encode(), \
                     b58encode(ed25519PkToCurve25519(signer.verraw)).encode()
    return (sigkey, verkey), (prikey, pubkey)


def isHex(val: str) -> bool:
    """
    Return whether the given str represents a hex value or not

    :param val: the string to check
    :return: whether the given str represents a hex value
    """
    if isinstance(val, bytes):
        # only decodes utf-8 string
        try:
            val = val.decode()
        except:
            return False
    return isinstance(val, str) and all(c in string.hexdigits for c in val)


def ed25519SkToCurve25519(sk, toHex=False):
    if isHex(sk):
        sk = unhexlify(sk)
    secretKey = ctypes.create_string_buffer(crypto_box_SECRETKEYBYTES)
    ret = nacl.crypto_sign_ed25519_sk_to_curve25519(secretKey, sk)
    if ret:
        raise Exception("error in converting ed22519 key to curve25519")
    return hexlify(secretKey.raw) if toHex else secretKey.raw


def ed25519PkToCurve25519(pk, toHex=False):
    if isHex(pk):
        pk = unhexlify(pk)
    publicKey = ctypes.create_string_buffer(crypto_box_PUBLICKEYBYTES)
    ret = nacl.crypto_sign_ed25519_pk_to_curve25519(publicKey, pk)
    if ret:
        raise Exception("error in converting ed22519 key to curve25519")
    return hexlify(publicKey.raw) if toHex else publicKey.raw


def getNodeListenerPort(name):
    index = int(name[4:])
    return 9700 + index


def getClientListenerPort(name):
    index = int(name[4:])
    return 9800 + index


def unow(inMsec=False):
    t = datetime.datetime.utcnow().timestamp()
    if inMsec:
        t *= 1000
    return int(t)


def benchMsg(size=1024):
    data = randomSeed(size).decode()
    return {
        'start': unow(inMsec=True),
        'data': data
    }


def getAllNodeNames():
    names = []
    restrNodeCount = config.RestrictedNodeCount if \
        config.RestrictedNodeCount and \
        isinstance(config.RestrictedNodeCount, int) else None
    from pzmq.curvy import Curvy
    for key_file in os.listdir(Curvy.publicKeysDir):
        if key_file.startswith("Node") and key_file.endswith(".key"):
            name = key_file.split('.')[0]
            if restrNodeCount:
                nodeNum = int(name[4:])
                if nodeNum > restrNodeCount:
                    continue
            names.append(name)
    return names
