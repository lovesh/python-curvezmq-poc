"""
This file contains IP of each node you want to run, if you want to run only
3 nodes, change the IP of only Node1, Node2 and Node3,
if you want to run 5 change ip of Node1 to Node5 and so on
"""


Node1 = '13.54.25.255'
Node2 = '13.54.72.155'
Node3 = '13.54.67.153'
Node4 = '13.54.5.240'
Node5 = '54.82.168.219'
Node6 = '54.85.150.93'
Node7 = '54.210.111.230'
Node8 = '54.145.5.39'
Node9 = '52.199.113.82'
Node10 = '52.197.255.217'
Node11 = '52.199.167.25'
Node12 = '52.198.131.156'
Node13 = '54.251.164.231'
Node14 = '54.255.144.15'
Node15 = '54.169.82.204'
