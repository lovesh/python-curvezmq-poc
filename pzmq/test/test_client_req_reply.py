import asyncio

from pzmq.test.helper import checkAllNodesConnected


def testClientReqReply(loop, nodes, client1):
    for node in nodes:
        asyncio.ensure_future(node.run())
    loop.run_until_complete(asyncio.sleep(5))
    checkAllNodesConnected(nodes)
    client1.start()
    asyncio.ensure_future(client1.run())
    loop.run_until_complete(asyncio.sleep(3))
    assert len(client1.connectedNodes) == len(nodes)
    assert len(client1.requests) == 1
    assert len(client1.replies) == 1

