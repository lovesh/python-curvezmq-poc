import json
import signal
import time
import zmq
from zmq.auth.thread import ThreadAuthenticator

from pzmq.util import randomSeed, getEd25519AndCurve25519Keys
from raet.nacling import Signer as NaclSigner, Verifier


def run(runFor=10, msgSize=200):
    msgs = {
        'alpha': [0, 0],
        'beta': [0, 0],
        'gamma': [0, 0],
    }
    sockets = {
        'alpha': [],
        'beta': [],
        'gamma': [],
    }

    ports = {
        'alpha': 9610,
        'beta': 9611,
        'gamma': 9612,
    }

    start = time.perf_counter()
    # print("Started Handshake at {}".format(start))
    ctx = zmq.Context.instance()

    def stop():
        print("Message stats over {} seconds: {}".format(runFor, msgs))

        # print("Finished\n")

    signal.signal(signal.SIGINT, lambda _, __: stop())

    for name in sockets:
        sock = ctx.socket(zmq.ROUTER)
        sock.bind('tcp://*:{}'.format(ports[name]))
        sockets[name].append(sock)

        for n in sockets:
            if name != n:
                sock = ctx.socket(zmq.DEALER)
                sock.identity = name.encode()
                sock.connect('tcp://127.0.0.1:{}'.format(ports[n]))
                sockets[name].append(sock)

    for name in sockets:
        sockets[name][1].send(b'1')
        sockets[name][2].send(b'2')
    time.sleep(.4)
    for name in sockets:
        sockets[name][0].recv_multipart()
        sockets[name][0].recv_multipart()

    handshakeDone = time.perf_counter()
    # print("Finished Handshakes in {}".format(handshakeDone - start))

    print("Starting message sending for {} sec, each message is {} bytes".
          format(runFor, msgSize))
    time.sleep(1)

    i = 0
    while time.perf_counter() - handshakeDone < runFor:
        i += 1
        for name in sockets:
            msg = {'data': '{}:{}'.format(i, randomSeed(msgSize).decode())}
            # Purposely doing twice, since RAET will do too
            try:
                sockets[name][1].send(json.dumps(msg).encode(), flags=zmq.NOBLOCK)
                msgs[name][0] += 1
            except zmq.Again:
                pass
            try:
                sockets[name][2].send(json.dumps(msg).encode(), flags=zmq.NOBLOCK)
                msgs[name][0] += 1
            except zmq.Again:
                pass

        for name in sockets:
            while True:
                try:
                    ident, msg = sockets[name][0].recv_multipart(flags=zmq.NOBLOCK)
                    msgs[name][1] += 1
                except zmq.Again:
                    break

    msgsDone = time.perf_counter()
    # print("Finished Sending Messages in {}".format(msgsDone - handshakeDone))

    incmgDone = time.perf_counter()
    # print("Finished Processing incoming Messages in {}".format(
    #     incmgDone - msgsDone))

    stop()


if __name__ == "__main__":
    run(10, 200)
    # for i in range(2, 11, 2):
    #     run(30, i*100)
    #     run(60, i*100)
