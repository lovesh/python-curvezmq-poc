import time
import zmq
from zmq.utils.monitor import recv_monitor_message


def getMonitorMsg(monitor):
    try:
        return recv_monitor_message(monitor, flags=zmq.NOBLOCK)
    except zmq.Again:
        return None


def test1():
    port = 14577
    ctx = zmq.Context.instance()
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.bind('tcp://*:{}'.format(port))
    sock2 = ctx.socket(zmq.DEALER)
    sock2.setsockopt(zmq.LINGER, 1)
    sock2.monitor("inproc://monitor.rep", zmq.EVENT_ALL)
    # create listening socket for monitor
    s_event = ctx.socket(zmq.PAIR)
    s_event.connect("inproc://monitor.rep")
    s_event.linger = 0
    sock2.connect("tcp://127.0.0.1:{}".format(port))
    time.sleep(.1)
    m = getMonitorMsg(s_event)
    if m['event'] == zmq.EVENT_CONNECT_DELAYED:
        print(m)
        assert m['endpoint'] == "tcp://127.0.0.1:{}".format(port).encode()
        # test receive event for connected event
    m = getMonitorMsg(s_event)
    print(m)
    assert m['event'] == zmq.EVENT_CONNECTED
    sock1.close()
    time.sleep(.1)
    m = getMonitorMsg(s_event)
    print(m)
    assert m['event'] == zmq.EVENT_DISCONNECTED
    time.sleep(.2)
    m = getMonitorMsg(s_event)
    print(m)
    assert m['event'] == zmq.EVENT_CONNECT_RETRIED
    sock2.close()
    s_event.close()

# test1()


def close(sock):
    sock.unbind(sock.LAST_ENDPOINT)
    sock.close()


def test2():
    port = 14578
    ctx = zmq.Context.instance()
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.setsockopt(zmq.ROUTER_MANDATORY, 1)
    sock1.bind('tcp://*:{}'.format(port))
    print('sock1 has fd {}'.format(sock1.FD))
    sock2 = ctx.socket(zmq.DEALER)
    sock2.setsockopt(zmq.LINGER, 1)
    sock2.connect("tcp://127.0.0.1:{}".format(port))
    print('sock2 has fd {}'.format(sock2.FD))
    sock2.send(b'11')
    print(sock1.recv_multipart())
    close(sock1)
    close(sock2)
    sock2 = ctx.socket(zmq.DEALER)
    sock2.setsockopt(zmq.LINGER, 1)
    sock2.connect("tcp://127.0.0.1:{}".format(port))
    print('sock2 has fd {}'.format(sock2.FD))
    sock2.send(b'22')
    time.sleep(5)
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.bind('tcp://*:{}'.format(port))
    print('sock1 has fd {}'.format(sock1.FD))
    print(sock1.recv_multipart())
    close(sock1)
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.bind('tcp://*:{}'.format(port))
    print('sock1 has fd {}'.format(sock1.FD))
    close(sock1)
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.bind('tcp://*:{}'.format(port))
    print('sock1 has fd {}'.format(sock1.FD))


# test2()


def test3():
    port = 14579
    ctx = zmq.Context.instance()
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.bind('tcp://*:{}'.format(port))
    close(sock1)
    # time.sleep(5)
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.bind('tcp://*:{}'.format(port))


# test3()


def test4():
    port = 14580
    ctx = zmq.Context.instance()
    sock1 = ctx.socket(zmq.ROUTER)
    addr = 'tcp://*:{}'.format(port)
    caddr = 'tcp://127.0.0.1:{}'.format(port)
    sock1.bind(addr)
    sock2 = ctx.socket(zmq.DEALER)
    sock2.identity = b'sock2'
    sock2.connect(caddr)
    sock3 = ctx.socket(zmq.DEALER)
    sock3.identity = b'sock3'
    sock3.connect(caddr)
    sock4 = ctx.socket(zmq.DEALER)
    sock4.identity = b'sock4'
    sock4.connect(caddr)
    for s in (sock2, sock3, sock4):
        s.send('{}>>'.format(s.identity).encode())
    for i in range(3):
        print(sock1.recv_multipart())
    close(sock1)
    sock2.close()
    sock3.close()
    sock4.close()

    time.sleep(1)

    sock2 = ctx.socket(zmq.DEALER)
    sock2.identity = b'sock2'
    sock2.connect(caddr)
    sock3 = ctx.socket(zmq.DEALER)
    sock3.identity = b'sock3'
    sock3.connect(caddr)
    sock4 = ctx.socket(zmq.DEALER)
    sock4.identity = b'sock4'
    sock4.connect(caddr)
    for s in (sock2, sock3, sock4):
        s.send('{}>>'.format(s.identity).encode())

    time.sleep(3)

    sock1 = ctx.socket(zmq.ROUTER)
    addr = 'tcp://*:{}'.format(port)
    sock1.bind(addr)
    for i in range(3):
        print(sock1.recv_multipart())

# test4()


def test5():
    port = 14581
    ctx = zmq.Context.instance()
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.bind('tcp://*:{}'.format(port))
    sock1.setsockopt(zmq.LINGER, 20)
    sock2 = ctx.socket(zmq.DEALER)
    sock2.setsockopt(zmq.LINGER, 20)
    monitorSock = sock2.get_monitor_socket()
    sock2.connect("tcp://127.0.0.1:{}".format(port))
    time.sleep(.1)
    m = getMonitorMsg(monitorSock)
    if m['event'] == zmq.EVENT_CONNECT_DELAYED:
        print(m)
        assert m['endpoint'] == "tcp://127.0.0.1:{}".format(port).encode()
    sock1.close()
    time.sleep(.3)
    # sock2.disable_monitor()
    sock2.monitor(None, 0)
    sock2._monitor_socket = None
    sock2.close()
    print(101)
    print(102)


# test5()


def test6():
    port = 14582
    ctx = zmq.Context.instance()
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.bind('tcp://*:{}'.format(port))
    sock2 = ctx.socket(zmq.DEALER)
    time.sleep(.1)
    monitorSock = sock2.get_monitor_socket()
    sock2.connect("tcp://127.0.0.1:{}".format(port))
    time.sleep(.1)
    m = getMonitorMsg(monitorSock)
    if m['event'] == zmq.EVENT_CONNECT_DELAYED:
        print(m)
        assert m['endpoint'] == "tcp://127.0.0.1:{}".format(port).encode()
    sock2.send(b'11', flags=zmq.NOBLOCK)
    time.sleep(.3)
    print(sock1.recv_multipart(flags=zmq.NOBLOCK))
    # sock2.disable_monitor()
    sock2.monitor(None, 0)
    sock2._monitor_socket = None
    sock2.close()
    r = sock1.unbind(sock1.LAST_ENDPOINT)
    print(r)
    sock1.close()
    print(101)
    time.sleep(.1)
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.bind('tcp://*:{}'.format(port))
    time.sleep(.1)
    print(102)
    sock2 = ctx.socket(zmq.DEALER)
    sock2.connect("tcp://127.0.0.1:{}".format(port))
    time.sleep(.1)
    sock2.send(b'12', flags=zmq.NOBLOCK)
    monitorSock = sock2.get_monitor_socket()
    getMonitorMsg(monitorSock)
    sock2.send(b'13', flags=zmq.NOBLOCK)
    time.sleep(.1)
    print(sock1.recv_multipart(flags=zmq.NOBLOCK))
    print(sock1.recv_multipart(flags=zmq.NOBLOCK))

test6()
