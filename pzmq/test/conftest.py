import asyncio
import os

import pytest
import zmq.asyncio

from pzmq.client import OldClient as Client
from pzmq.node import OldNode as Node
from pzmq.curvy import Curvy
from pzmq.util import getAllNodeNames


@pytest.yield_fixture()
def loop():
    loop = zmq.asyncio.ZMQEventLoop()
    loop.set_debug(True)
    asyncio.set_event_loop(loop)
    yield loop
    pending = asyncio.Task.all_tasks()
    pendingTasks = asyncio.gather(*pending)
    try:
        f = loop.run_until_complete(pendingTasks)
    except asyncio.CancelledError as e:
    # except Exception as e:
        print(e)
    loop.close()
    # try:
    #     loop.run_until_complete(pendingTasks)
    # except BaseException as e:
    #     pendingTasks.cancel()
    #     # loop.run_forever()
    #     # pendingTasks.exception()
    # finally:
    #     loop.close()


@pytest.yield_fixture()
def nodes():
    nodes = []
    for n in getAllNodeNames():
        node = Node(n)
        node.start()
        nodes.append(node)
    yield nodes
    for node in nodes:
        node.stop()


@pytest.yield_fixture()
def client1():
    client = Client('c1')
    yield client
    client.stop()
