import asyncio
from pzmq.node import OldNode as Node
from pzmq.client import OldClient as Client
from pzmq.test.helper import checkAllNodesConnected


def testNodesConnectAndSendPing(loop, nodes):
    for node in nodes:
        asyncio.ensure_future(node.run())
    loop.run_until_complete(asyncio.sleep(2))
    checkAllNodesConnected(nodes)