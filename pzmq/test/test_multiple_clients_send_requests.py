import asyncio

from pzmq.client import OldClient as Client
from pzmq.test.helper import checkAllNodesConnected


def testMultipleClientsReqReply(loop, nodes):
    for node in nodes:
        asyncio.ensure_future(node.run())
    loop.run_until_complete(asyncio.sleep(20))
    checkAllNodesConnected(nodes)
    clients = []
    for i in range(1, 11):
        client = Client('c{}'.format(i))
        clients.append(client)
        client.start()
        asyncio.ensure_future(client.run())

    loop.run_until_complete(asyncio.sleep(30))
    for c in clients:
        c.stop()
