import time
import zmq


class Server:
    def __init__(self, port):
        self.ctx = zmq.Context.instance()
        self.sock = self.ctx.socket(zmq.ROUTER)
        self.sock.bind('tcp://*:{}'.format(port))

    def close(self):
        self.sock.close()


class Client:
    def __init__(self, port):
        self.ctx = zmq.Context.instance()
        self.sock = self.ctx.socket(zmq.DEALER)
        self.sock.setsockopt(zmq.LINGER, 1)
        self.sock.setsockopt(zmq.HEARTBEAT_IVL, 50)
        self.sock.setsockopt(zmq.HEARTBEAT_TTL, 110)
        self.sock.setsockopt(zmq.HEARTBEAT_TIMEOUT, 300)
        self.sock.connect('tcp://127.0.0.1:{}'.format(port))

    def send(self, msg):
        try:
            self.sock.send(b'm:2', flags=zmq.NOBLOCK)
        except zmq.Again as ex:
            print('got exc {}'.format(ex))


def testH():
    # check fail to disconnect
    port = 14577
    server = Server(port)
    client = Client(port)
    time.sleep(.5)
    client.send(b'm:1')
    print(server.sock.recv_multipart())
    client.send(b'm:2')
    print(server.sock.recv_multipart())
    server.close()
    client.send(b'm:3')
    time.sleep(2)
    client.send(b'm:4')
    print(1)


# testH()


def test1():
    print('On zmq_version {}'.format(zmq.zmq_version()))
    print('On zmq.__version__ {}'.format(zmq.__version__))
    port = 8876
    ctx = zmq.Context.instance()
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.setsockopt(zmq.LINGER, 1)
    sock1.bind('tcp://*:{}'.format(port))
    sock2 = ctx.socket(zmq.DEALER)
    sock2.setsockopt(zmq.LINGER, 1)
    sock2.connect('tcp://127.0.0.1:{}'.format(port))
    sock2.send(b'm_1')
    sock2.send(b'm_2')
    print(sock1.recv_multipart())
    print(sock1.recv_multipart())
    sock1.close()
    sock2.close()
    ctx.term()
    print('going down')


# test1()


def test2():
    port = 8876
    ctx = zmq.Context.instance()
    sock1 = ctx.socket(zmq.ROUTER)
    sock1.setsockopt(zmq.LINGER, 1)
    sock2 = ctx.socket(zmq.DEALER)
    sock2.setsockopt(zmq.LINGER, 1)
    sock2.connect('tcp://127.0.0.1:{}'.format(port))
    r1 = sock2.send(b'm_1', flags=zmq.NOBLOCK)
    r2 = sock2.send(b'm_2', flags=zmq.NOBLOCK)
    time.sleep(2)
    sock1.bind('tcp://*:{}'.format(port))
    r3 = sock2.send(b'm_3')
    print(sock1.recv_multipart())
    print(sock1.recv_multipart())
    r4 = sock2.send(b'm_4')
    print(sock1.recv_multipart())
    print(sock1.recv_multipart())
    print(r1, r2, r3, r4)

# test2()


def test3():
    port = 8879
    ctx = zmq.Context.instance()
    listener = ctx.socket(zmq.ROUTER)
    listener.setsockopt(zmq.LINGER, 1)
    listener.bind('tcp://*:{}'.format(port))
    fds = set()
    und = set()
    socks = []
    count = 100
    for i in range(count):
        sock = ctx.socket(zmq.DEALER)
        sock.setsockopt(zmq.LINGER, 1)
        sock.connect('tcp://127.0.0.1:{}'.format(port))
        print(sock.FD)
        fds.add(sock.FD)
        und.add(sock.underlying)
        sock.send('m:{}:{}:{}'.format(i, sock.FD, sock.underlying).encode())
        # socks.append(sock)
        time.sleep(.001)
    print(len(fds), len(und))
    r = 0
    while r != count:
        try:
            print(listener.recv_multipart(flags=zmq.NOBLOCK))
            r += 1
        except:
            time.sleep(.01)
            continue
    print('got all')
    # for s in socks:
    #     s.close()
    # sock1.close()
    print('done')


test3()


def test4():
    port = 8879
    ctx = zmq.Context.instance()
    listener = ctx.socket(zmq.ROUTER)
    listener.setsockopt(zmq.LINGER, 1)
    listener.bind('tcp://*:{}'.format(port))
    fds = set()
    und = set()
    socks = []
    count = 20
    for i in range(count):
        sock = ctx.socket(zmq.DEALER)
        sock.setsockopt(zmq.LINGER, 1)
        sock.connect('tcp://127.0.0.1:{}'.format(port))
        print(sock.FD)
        fds.add(sock.FD)
        und.add(sock.underlying)
        sock.send('m:{}:{}:{}'.format(i, sock.FD, sock.underlying).encode())
        # socks.append(sock)
        # time.sleep(.01)
    print(len(fds), len(und))
    got = set()
    start = time.perf_counter()
    while len(got) != count and (time.perf_counter() - start) < 5:
        try:
            m = listener.recv_multipart(flags=zmq.NOBLOCK)
            print(m)
            got.add(int(m[1].split(b':')[1]))
        except:
            time.sleep(.01)
            continue
    if len(got) == count:
        print('got all')
        # for s in socks:
        #     s.close()
        # sock1.close()
        print('done')
    else:
        print('retrying, got only ', got)
        for i in set(range(count)).difference(got):
            sock = ctx.socket(zmq.DEALER)
            sock.setsockopt(zmq.LINGER, 1)
            sock.connect('tcp://127.0.0.1:{}'.format(port))
            print(sock.FD)
            fds.add(sock.FD)
            und.add(sock.underlying)
            sock.send('n:{}:{}:{}'.format(i, sock.FD, sock.underlying).encode())
        start = time.perf_counter()
        while len(got) != count and (time.perf_counter() - start) < 5:
            try:
                m = listener.recv_multipart(flags=zmq.NOBLOCK)
                print(m)
                got.add(int(m[1].split(b':')[1]))
            except:
                time.sleep(.01)
                continue

        if len(got) == count:
            print('got all')
            # for s in socks:
            #     s.close()
            # sock1.close()
            print('done')


# test4()
