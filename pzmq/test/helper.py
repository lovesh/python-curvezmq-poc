def checkNodeConnectedToAll(node, allNodes):
    expectedConnecttions = {n.name for n in allNodes if n.name != node.name}
    assert len(node.connectedNodes) == (
    len(allNodes) - 1), '{} is disconnected from {}'.format(node, ','.join(
        expectedConnecttions.difference(node.connectedNodes)))


def checkAllNodesConnected(allNodes):
    for node in allNodes:
        checkNodeConnectedToAll(node, allNodes)
