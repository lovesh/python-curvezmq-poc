import json
import signal
import time
import zmq
from zmq.auth.thread import ThreadAuthenticator

from pzmq.util import randomSeed, getEd25519AndCurve25519Keys
from raet.nacling import Signer, Verifier


def run(runFor=10, msgSize=200):
    msgs = {
        'alpha': [0, 0],
        'beta': [0, 0],
        'gamma': [0, 0],
    }
    sockets = {
        'alpha': [],
        'beta': [],
        'gamma': [],
    }
    keys = {
        'alpha': list(map(str.encode, ['lm6sNo{c:{=J6[TeaXA<0*>fyzw<a[e1A[$JmvOY', '/IfCsCrMIA02G)w915rmD[gqIBsuYXjigkuzOtmb'])),
        'beta': list(map(str.encode, ['nV.AZ6>Z@*HR[&jFsrxqKrhynt0?nH.aT<U1)i91', '>PSg^p6l{METhR)YsyilBi#Bvrju-CtAEJocFK5F'])),
        'gamma': list(map(str.encode, ['hW#lt6E/DG=Z8[VqMHy#-=9=w}F@FoSU>PDOY^lY', 'ZYvez2rzvEz?XF#Q!YV*Q2#T3]<s*CZ<M&rFksM}'])),
    }

    # Total time spent in signing
    signingTime = 0
    # Total time spent in signature verification
    verificationTime = 0

    # keys = {}
    sigkeys = {}
    signers = {}
    verifiers = {}
    for name in sockets:
        (sigkey, verkey), (prikey, pubkey) = getEd25519AndCurve25519Keys()
        # keys[name] = [pubkey, prikey]
        sigkeys[name] = [verkey, sigkey]
        signers[name] = Signer(sigkey)
        verifiers[name] = Verifier(verkey)

    def sign(msg, by):
        return signers[by].signature(msg)

    def prepMsg(msg, by):
        nonlocal signingTime
        start = time.perf_counter()
        sig = sign(msg, by)
        signingTime += time.perf_counter() - start
        return msg + sig

    def verify(msg, by):
        nonlocal verificationTime
        start = time.perf_counter()
        r = verifiers[by].verify(msg[-64:], msg[:-64])
        verificationTime += time.perf_counter() - start
        return r

    ports = {
        'alpha': 9610,
        'beta': 9611,
        'gamma': 9612,
    }

    start = time.perf_counter()
    # print("Started Handshake at {}".format(start))
    ctx = zmq.Context.instance()

    # Start an authenticator for this context.
    auth = ThreadAuthenticator(ctx)
    auth.start()
    auth.allow('127.0.0.1')
    # Tell authenticator to use the certificate in a directory
    auth.configure_curve(domain='*', location=zmq.auth.CURVE_ALLOW_ANY)

    def stop():
        # stop auth thread
        auth.stop()
        print("Message stats over {} seconds: {}".format(runFor, msgs))
        print("Time spent in signing {} seconds".format(signingTime))
        print("Time spent in verification  {} seconds".format(verificationTime))
        # print("Finished\n")

    signal.signal(signal.SIGINT, lambda _, __: stop())

    for name in sockets:
        sock = ctx.socket(zmq.ROUTER)
        sock.curve_publickey = keys[name][0]
        sock.curve_secretkey = keys[name][1]
        sock.curve_server = True
        sock.bind('tcp://*:{}'.format(ports[name]))
        sockets[name].append(sock)

        for n in sockets:
            if name != n:
                sock = ctx.socket(zmq.DEALER)
                sock.curve_publickey = keys[name][0]
                sock.curve_secretkey = keys[name][1]
                sock.curve_serverkey = keys[n][0]
                sock.identity = name.encode()
                sock.connect('tcp://127.0.0.1:{}'.format(ports[n]))
                sockets[name].append(sock)

    for name in sockets:
        sockets[name][1].send(b'1')
        sockets[name][2].send(b'2')
    time.sleep(.4)
    for name in sockets:
        sockets[name][0].recv_multipart()
        sockets[name][0].recv_multipart()

    handshakeDone = time.perf_counter()
    # print("Finished Handshakes in {}".format(handshakeDone - start))

    print("Starting message sending for {} sec, each message is {} bytes".
          format(runFor, msgSize))
    time.sleep(1)

    i = 0
    while time.perf_counter() - handshakeDone < runFor:
        i += 1
        for name in sockets:
            msg = {'data': '{}:{}'.format(i, randomSeed(msgSize).decode())}
            # Purposely doing twice, since RAET will do too
            try:
                sockets[name][1].send(prepMsg(json.dumps(msg).encode(), name), flags=zmq.NOBLOCK)
                msgs[name][0] += 1
            except zmq.Again:
                pass
            try:
                sockets[name][2].send(prepMsg(json.dumps(msg).encode(), name), flags=zmq.NOBLOCK)
                msgs[name][0] += 1
            except zmq.Again:
                pass

        for name in sockets:
            while True:
                try:
                    ident, msg = sockets[name][0].recv_multipart(flags=zmq.NOBLOCK)
                    assert verify(msg, ident.decode())
                    msgs[name][1] += 1
                except zmq.Again:
                    break

    msgsDone = time.perf_counter()
    # print("Finished Sending Messages in {}".format(msgsDone - handshakeDone))

    incmgDone = time.perf_counter()
    # print("Finished Processing incoming Messages in {}".format(
    #     incmgDone - msgsDone))

    stop()


if __name__ == "__main__":
    run(10, 200)
    # for i in range(2, 11, 2):
    #     run(30, i*100)
    #     run(60, i*100)
