import time
from itertools import combinations

import ioflo
from ioflo.base.consoling import getConsole


import raet
from raet import raeting
from raet.raeting import AutoMode

from pzmq.util import randomSeed

console = getConsole()
console.reinit(verbosity=console.Wordage.mute)


def serviceStacks(stacks, duration=1.0, period=0.1):
    '''
    Utility method to service queues. Call from test method.
    '''
    store = ioflo.base.storing.Store(stamp=0.0)
    timer = ioflo.aid.timing.StoreTimer(store=store, duration=duration)
    while not timer.expired:
        for stack in stacks:
            stack.serviceAll()
            stack.store.advanceStamp(period)

        store.advanceStamp(period)
        if all([not stack.transactions for stack in stacks]):
            break
        # time.sleep(1.0)


def run(runFor=10, msgSize=200):
    msgs = {
        'alpha': [0, 0],
        'beta': [0, 0],
        'gamma': [0, 0],
    }

    try:
        start = time.perf_counter()
        # print("Started Handshakes at {}".format(start))
        alpha = raet.road.stacking.RoadStack(name='alpha',
                                             ha=('0.0.0.0', 7531),
                                             main=True,
                                             auto=AutoMode.always.value)

        beta = raet.road.stacking.RoadStack(name='beta',
                                            ha=('0.0.0.0', 7532),
                                            main=True,
                                            auto=AutoMode.always.value)

        gamma = raet.road.stacking.RoadStack(name='gamma',
                                            ha=('0.0.0.0', 7533),
                                            main=True,
                                            auto=AutoMode.always.value)

        stacks = [alpha, beta, gamma]
        # serviceStacks(stacks)
        # for stack in stacks:
        #     stack.keep.clearAllDir()  # clear persisted data
        # serviceStacks(stacks)

        remote = raet.road.estating.RemoteEstate(stack=alpha,
                                                 name=beta.name,
                                                 ha=beta.ha)
        alpha.addRemote(remote)
        alpha.join(uid=remote.uid, cascade=True)

        remote = raet.road.estating.RemoteEstate(stack=alpha,
                                                 name=gamma.name,
                                                 ha=gamma.ha)
        alpha.addRemote(remote)
        alpha.join(uid=remote.uid, cascade=True)

        remote = raet.road.estating.RemoteEstate(stack=beta,
                                                 name=gamma.name,
                                                 ha=gamma.ha)
        beta.addRemote(remote)
        beta.join(uid=remote.uid, cascade=True)

        serviceStacks(stacks)
        handshakeDone = time.perf_counter()
        # print("Finished Handshakes in {}".format(handshakeDone - start))

        print("Starting message sending for {} sec, each message is {} bytes".
              format(runFor, msgSize))
        i = 0
        while time.perf_counter() - handshakeDone < runFor:
            i += 1
            for s in stacks:
                msg = {'data': '{}:{}'.format(i, randomSeed(msgSize).decode())}
                for remote in s.remotes.values():
                    s.transmit(msg, remote.uid)
                    msgs[s.name][0] += 1
            if (i % 1000) == 0:
                serviceStacks(stacks, duration=10)
        serviceStacks(stacks, duration=10)
        msgsDone = time.perf_counter()
        print("Finished Sending Messages in {}, ran loop {} times".format(msgsDone - handshakeDone, i))

        for stack in stacks:
            msgs[stack.name][1] = len(stack.rxMsgs)

        incmgDone = time.perf_counter()
        # print("Finished Processing incoming Messages in {}".format(incmgDone - msgsDone))

    except Exception as e:
        print("Got exception {}".format(e))
        pass
    finally:
        for stack in stacks:
            stack.server.close()  # close the UDP socket
            stack.keep.clearAllDir()  # clear persisted data

        print("Message stats over {} seconds: {}".format(runFor, msgs))

        # print("Finished\n")


if __name__ == "__main__":
    run(10, 200)
    # for i in range(2, 11, 2):
    #     run(30, i*100)
    #     run(60, i*100)