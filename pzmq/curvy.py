import os

import zmq
import zmq.auth

from pzmq import config
from pzmq.util import getAllNodeNames


class Curvy:
    # Both nodes and clients extend it

    baseDir = os.path.dirname(__file__)
    keysDir = os.path.join(baseDir, 'certificates')
    publicKeysDir = os.path.join(baseDir, 'public_keys')
    secretKeysDir = os.path.join(baseDir, 'private_keys')

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name

    @property
    def nodesAndKeys(self):
        keys = {name: self.getPublicKey(name) for name in getAllNodeNames()
                if name != self.name}
        return keys

    @property
    def selfKeys(self):
        serverSecretFile = os.path.join(self.secretKeysDir,
                                          "{}.key_secret".format(self.name))
        return zmq.auth.load_certificate(serverSecretFile)

    @staticmethod
    def getPublicKey(name):
        serverPublicFile = os.path.join(Curvy.publicKeysDir, "{}.key".format(name))
        serverPublic, _ = zmq.auth.load_certificate(serverPublicFile)
        return serverPublic
