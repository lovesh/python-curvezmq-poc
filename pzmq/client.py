import signal
from time import sleep, time
import sys

import asyncio
import zmq
import zmq.asyncio
from zmq.utils import z85

from pzmq import config
from pzmq.authenticator import MyThreadAuthenticator, AsyncioAuthenticator
from pzmq.connects_to_nodes import ConnectsToNodes
from pzmq.curvy import Curvy
from pzmq.util import getClientListenerPort, unow, benchMsg


class OldClient(Curvy, ConnectsToNodes):
    def __init__(self, name, reqDelay=10):
        Curvy.__init__(self, name)
        ConnectsToNodes.__init__(self)

        self.name = name

        self.poller = zmq.asyncio.Poller()

        # Context for socket used for listening to nodes
        self.nodeCtx = zmq.asyncio.Context.instance()

        # Map of name to socket
        self.otherNodes = {}
        # Set of connected node names
        self.connectedNodes = set()

        # Map of request id to map of sender to sent ack
        self.requests = {}
        # Map of request id to map of sender to sent reply
        self.replies = {}

        self.nodeAuth = AsyncioAuthenticator(self.nodeCtx)
        self.nodeAuth.start()
        self.isRunning = False
        self.nodeAuth.allow('127.0.0.1')
        # Tell authenticator to use the certificate in a directory
        self.nodeAuth.configure_curve(domain='*', location=self.publicKeysDir)
        self.setupNodes()
        self.lastRequestAt = None

        self.stats = {}
        self.reqDelay = reqDelay

    def setupNodes(self):
        for name, pubkey in self.nodesAndKeys.items():
            sock = self.connectNode(name, pubkey, False)
            self.poller.register(sock, zmq.POLLIN)

    @asyncio.coroutine
    def pingNodes(self, nodes=None):
        public, _ = self.selfKeys
        msg = {'type': 'ping', 'payload': '<My name is {}>. My pubkey is {}'.format(self, z85.encode(public))}
        yield from self.sendToNodes(msg, nodes)

    @asyncio.coroutine
    def pongNodes(self, nodes=None):
        public, _ = self.selfKeys
        msg = {'type': 'pong', 'payload': '<My name is {}>. My pubkey is {}'.format(self, z85.encode(public))}
        yield from self.sendToNodes(msg, nodes)

    @asyncio.coroutine
    def sendToNodes(self, msg, nodes=None):
        sentTo = []
        for name, sock in self.otherNodes.items():
            if not nodes or name in nodes:
                yield from sock.send_json(msg, flags=zmq.NOBLOCK)
                sentTo.append(name)
        return sentTo

    @asyncio.coroutine
    def recv(self, socks=None):
        for name, sock in self.otherNodes.items():
            if not socks or (sock in socks and socks[sock] == zmq.POLLIN):
                msg = yield from sock.recv_json(flags=zmq.NOBLOCK)
                print('{} sent {}'.format(name, msg))
                typ = msg.get('type')
                if typ == 'ping':
                    self.connectedNodes.add(name)
                    yield from self.pongNodes(nodes=[name, ])
                if typ == 'pong':
                    self.connectedNodes.add(name)
                if typ in ('ack', 'reply'):
                    reqId = msg.get('payload').get('id')
                    if reqId not in self.requests:
                        raise RuntimeError('{} did not send {}'.format(self,
                                                                       reqId))
                    coll = self.requests if typ == 'ack' else self.replies
                    coll[reqId][name] = msg
                    if typ not in self.stats[reqId]:
                        self.stats[reqId][typ] = {}
                    self.stats[reqId][typ][name] = unow(inMsec=True)

    @asyncio.coroutine
    def sendReq(self):
        if len(self.connectedNodes) == len(self.otherNodes):
            if not self.lastRequestAt or self.lastRequestAt + self.reqDelay < time():
                reqId = int(time()*1000000000)
                data = {
                    'id': reqId,
                    'sender': self.name,
                    'extra': benchMsg(config.ClientReqSize),
                }
                msg = {'type': 'req', 'payload': data}
                sentTo = yield from self.sendToNodes(msg)
                if sentTo:
                    self.requests[reqId] = {}
                    self.replies[reqId] = {}
                    self.lastRequestAt = time()
                    self.stats[reqId] = {
                        'sentAt': unow(inMsec=True)
                    }

    def start(self):
        self.isRunning = True
        signal.signal(signal.SIGINT, lambda _, __: self.stop())
        self.started = unow()

    def stop(self):
        if self.isRunning:
            now = unow()
            self.isRunning = False
            for sock, _ in self.poller.sockets:
                self.poller.unregister(sock)
            for sock in self.otherNodes.values():
                try:
                    sock.close()
                except asyncio.CancelledError:
                    # TODO: Complete pending sends and receives.
                    pass
            self.nodeAuth.stop()
            # self.nodeCtx.term()
            a, r = self.showStats()
            print('{} stopping at {}'.format(self, now))
            return a, r

    def showStats(self):
        print('{} started at {}'.format(self, self.started))
        print('{} sent {} requests in {} sec'.format(self, len(self.stats),
                                                     unow()-self.started))
        # totalAcks = sum(len(d.get('ack', [])) for d in self.stats.values())
        # totalReps = sum(len(d.get('reply', [])) for d in self.stats.values())
        totalAcks, totalReps = 0, 0
        ackByNodes = {}
        replyByNodes = {}
        for data in self.stats.values():
            acks = data.get('ack', [])
            reps = data.get('reply', [])
            totalAcks += len(acks)
            totalReps += len(reps)
            for ack in acks:
                ackByNodes[ack] = ackByNodes.get(ack, 0) + 1
            for rep in reps:
                replyByNodes[rep] = replyByNodes.get(rep, 0) + 1

        print('{} got {} acks'.format(self, totalAcks))
        print('{} got {} replies'.format(self, totalReps))
        for nm, count in ackByNodes.items():
            print('{} got {} acks by {}'.format(self, count, nm))
        for nm, count in replyByNodes.items():
            print('{} got {} reply by {}'.format(self, count, nm))
        return totalAcks, totalReps

    @asyncio.coroutine
    def run(self):
        while self.isRunning:
            fut = yield from self.poller.poll(500)
            socks = dict(fut)
            yield from self.maintainNodeConnections()
            socks = {sock: ev for sock, ev in socks.items() if ev == zmq.POLLIN}
            if socks:
                yield from self.recv(socks)
            yield from self.sendReq()


if __name__ == '__main__':
    loop = zmq.asyncio.ZMQEventLoop()
    asyncio.set_event_loop(loop)

    name = sys.argv[1]
    client = OldClient(name)
    client.start()
    loop.run_until_complete(client.run())
