import json
import signal
from time import sleep
from typing import Tuple
import sys

import asyncio
import zmq
import zmq.asyncio
from zmq.auth.thread import ThreadAuthenticator

from pzmq import config
from pzmq.authenticator import MyThreadAuthenticator, AsyncioAuthenticator
from pzmq.connects_to_nodes import ConnectsToNodes
from pzmq.curvy import Curvy
from pzmq.util import getNodeListenerPort, getClientListenerPort, unow


class OldNode(Curvy, ConnectsToNodes):
    def __init__(self, name):
        Curvy.__init__(self, name)
        ConnectsToNodes.__init__(self)
        self.name = name
        # Context for socket used for listening to nodes, different from client
        #  since node-to-client communication is promiscuous
        self.nodeCtx = zmq.asyncio.Context.instance()

        self.clientCtx = zmq.asyncio.Context.instance()

        # Map of node name to socket
        self.otherNodes = {}

        # Map of node name to identity
        self.nodeIds = {}

        # Set of connected node names
        self.connectedNodes = set()

        # Map of connected client pubkey to socket
        self.connectedClients = {}

        # Map of request id to client identifier
        self.requests = {}

        # Map of message id to set of senders
        self.messages = {}

        self.nodeAuth = AsyncioAuthenticator(self.nodeCtx)
        self.nodeAuth.start()
        self.nodeAuth.allow('0.0.0.0')
        self.nodeAuth.configure_curve(domain='*', location=self.publicKeysDir)

        self.poller = zmq.asyncio.Poller()

        self.nodeTalker = self.nodeCtx.socket(zmq.ROUTER)
        self.poller.register(self.nodeTalker, zmq.POLLIN)
        self.setupNodes()

        self.clientAuth = AsyncioAuthenticator(self.clientCtx)
        self.clientAuth.start()
        self.clientAuth.allow('0.0.0.0')
        self.clientAuth.configure_curve(domain='*', location=zmq.auth.CURVE_ALLOW_ANY)

        self.clientListener = self.clientCtx.socket(zmq.ROUTER)
        self.setupClientListener()

        # self.reqDealer = self.clientCtx.socket(zmq.DEALER)
        # self.reqDealer.bind('inproc://reqDealer')
        #
        # zmq.proxy(self.clientListener, self.reqDealer)
        # self.worker = self.clientCtx.socket(zmq.DEALER)
        # self.worker.connect('inproc://reqDealer')
        self.isRunning = False
        self.stats = {}

    def getNodeName(self, ident):
        for n, i in self.nodesAndKeys.items():
            if i == ident:
                return n

    def setupClientListener(self):
        public, secret = self.selfKeys
        self.clientListener.curve_secretkey = secret
        self.clientListener.curve_publickey = public
        self.clientListener.curve_server = True
        self.clientListener.bind(
            'tcp://*:{}'.format(getClientListenerPort(self.name)))
        self.poller.register(self.clientListener, zmq.POLLIN)

    def setupNodes(self):
        public, secret = self.selfKeys
        self.nodeTalker.curve_secretkey = secret
        self.nodeTalker.curve_publickey = public
        self.nodeTalker.curve_server = True
        for name, pubkey in self.nodesAndKeys.items():
            self.connectNode(name, pubkey, True)
        self.nodeTalker.bind(
            'tcp://*:{}'.format(getNodeListenerPort(self.name)))

    @asyncio.coroutine
    def pingNodes(self, nodes=None):
        msg = {'type': 'ping', 'payload': '<My name is {}>'.format(self)}
        yield from self.sendToNodes(msg, nodes=nodes)

    @asyncio.coroutine
    def pongNodes(self, nodes=None):
        msg = {'type': 'pong', 'payload': '<My name is {}>'.format(self)}
        yield from self.sendToNodes(msg, nodes=nodes)

    @asyncio.coroutine
    def recv(self):
        ident, msg = yield from self.nodeTalker.recv_multipart(flags=zmq.NOBLOCK)
        msg = json.loads(msg.decode())
        typ = msg.get('type')
        name = self.getNodeName(ident)
        print('{} received {} from {}'.format(self, msg, name))
        if typ == 'ping':
            self.connectedNodes.add(name)
            yield from self.pongNodes(nodes=[name, ])
        if typ == 'pong':
            self.connectedNodes.add(name)
        if typ == 'cons' and config.NodesBroadcast:
            reqId = msg['payload']['id']
            if reqId not in self.messages:
                self.messages[reqId] = {}
            self.messages[reqId][name] = msg
            if reqId not in self.stats:
                self.stats[reqId] = {}
            if 'cons' not in self.stats[reqId]:
                self.stats[reqId]['cons'] = {}
            self.stats[reqId]['cons'][name] = unow(inMsec=True)
            yield from self.replyToClient(reqId, msg['payload'])

    @asyncio.coroutine
    def dealWithClients(self):
        try:
            # ident, msg = self.worker.recv_multipart(flags=zmq.NOBLOCK)
            # self.worker.send_multipart([ident, msg])
            ident, msg = yield from self.clientListener.recv_multipart(flags=zmq.NOBLOCK)
            print('Client {} sent {}'.format(ident, msg))
            msg = json.loads(msg.decode())
            typ = msg.get('type')
            if typ == 'ping':
                resp = json.dumps(msg).encode()
                yield from self.clientListener.send_multipart([ident, resp])
            if typ == 'req':
                reqId = msg['payload']['id']
                msg = {'type': 'ack', 'payload': msg['payload']}
                resp = json.dumps(msg).encode()
                yield from self.clientListener.send_multipart([ident, resp])
                self.requests[reqId] = ident
                if config.NodesBroadcast:
                    if reqId not in self.messages:
                        self.messages[reqId] = {}
                    msg = {'type': 'cons', 'payload': msg['payload']}
                    self.messages[reqId][self.name] = msg
                    yield from self.sendToNodes(msg)
                yield from self.replyToClient(reqId, msg['payload'])
        except zmq.Again:
            pass

    @asyncio.coroutine
    def replyToClient(self, reqId, payload):
        if not config.NodesBroadcast or len(self.messages[reqId]) == len(self.otherNodes) + 1:
            resp = json.dumps({'type': 'reply',
                               'payload': payload}).encode()
            yield from self.clientListener.send_multipart([self.requests[reqId], resp])
            self.cleanup(reqId)

    @asyncio.coroutine
    def sendToNodes(self, msg, nodes=None):
        for name, sock in self.otherNodes.items():
            if not nodes or name in nodes:
                try:
                    yield from sock.send_json(msg, flags=zmq.NOBLOCK)
                except zmq.Again:
                    print('{} tried sending {}'.format(self, msg))
                    pass

    def cleanup(self, reqId):
        if config.NodesBroadcast:
            if len(self.messages[reqId]) >= (len(self.otherNodes) + 1):
                self.messages.pop(reqId)
                self.requests.pop(reqId)
        else:
            self.requests.pop(reqId)

    def start(self):
        self.isRunning = True
        signal.signal(signal.SIGINT, lambda _, __: self.stop())
        self.started = unow()

    def stop(self):
        if self.isRunning:
            self.isRunning = False
            for sock, _ in self.poller.sockets:
                self.poller.unregister(sock)
            try:
                self.nodeTalker.close()
            except asyncio.CancelledError:
                # TODO: Complete pending sends and receives.
                pass
            try:
                self.clientListener.close()
            except asyncio.CancelledError:
                # TODO: Complete pending sends and receives.
                pass
            self.nodeAuth.stop()
            self.clientAuth.stop()
            # self.nodeCtx.term()
            # self.clientCtx.term()
            self.showStats()
            print('{} stopping at {}'.format(self, unow()))

    def showStats(self):
        print('{} started at {}'.format(self, self.started))
        print('{} received {} requests in {} sec'.format(self, len(self.requests),
                                                     unow() - self.started))
        # totalCons = sum(len(d.get('cons', [])) for d in self.stats.values())
        totalCons = 0
        consByNodes = {}
        for data in self.stats.values():
            cons = data.get('cons', [])
            totalCons += len(cons)
            for con in cons:
                consByNodes[con] = consByNodes.get(con, 0) + 1
        print('{} got {} cons'.format(self, totalCons))
        for nm, count in consByNodes.items():
            print('{} got {} cons by {}'.format(self, count, nm))

    @asyncio.coroutine
    def run(self):
        while self.isRunning:
            fut = yield from self.poller.poll(500)
            socks = dict(fut)
            yield from self.maintainNodeConnections()
            if self.nodeTalker in socks and socks[self.nodeTalker] == zmq.POLLIN:
                yield from self.recv()
            if self.clientListener in socks and socks[self.clientListener] == zmq.POLLIN:
                yield from self.dealWithClients()
            # sleep(.5)

if __name__ == '__main__':
    loop = zmq.asyncio.ZMQEventLoop()
    loop.set_debug(True)
    asyncio.set_event_loop(loop)

    name = sys.argv[1]
    node = OldNode(name)
    node.start()
    loop.run_until_complete(node.run())
