# If True, ignore AWS nodes mentioned in locations.py
RunLocalNodes = True

# If True, nodes send message of type `cons` to each other before sending reply.
# It will not send reply unless it has got `cons` message from all nodes
NodesBroadcast = True

# If set to None, will assume the node size of cluster to be the number of
# `Node` public keys present in `public_keys`. If given a number then will only
# pick nodes till that number, so if 3 is given, it will pick: Node1, Node2 and
# Node3 and run only these 3 nodes
RestrictedNodeCount = None

# Min. number of bytes in client request. The actual number of bytes would be
# a about 50 more. Used for benchmarking.
ClientReqSize = 100

