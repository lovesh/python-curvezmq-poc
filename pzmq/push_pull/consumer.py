import time
import zmq
import random


def consumer():
    consumer_id = random.randrange(1, 10005)
    print("I am consumer #%s" % (consumer_id))
    context = zmq.Context()

    consumer_receiver = context.socket(zmq.PULL)
    consumer_receiver.connect("tcp://127.0.0.1:5557")

    while True:
        try:
            work = consumer_receiver.recv_json(flags=zmq.NOBLOCK)
        except Exception as ex:
            print(ex)
            time.sleep(.1)
            continue
        data = work['num']
        result = {'consumer': consumer_id, 'num': data}
        print('{} received work {}'.format(consumer_id, work))

consumer()
