import random
import time
import zmq


def producer():
    producer_id = random.randrange(1, 10005)
    print("I am producer #%s" % (producer_id))
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.bind("tcp://127.0.0.1:5557")
    # Start your result manager and workers before you start your producers
    start = int(time.time())
    for num in range(10):
        work_message = { 'num' : start+num }
        print('{} sending {}'.format(producer_id, work_message))
        while True:
            try:
                zmq_socket.send_json(work_message, zmq.NOBLOCK)
                break
            except Exception as ex:
                print(ex)
                time.sleep(.1)
producer()
